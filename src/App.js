import React from "react";
import { dark, light } from "./config/theme.config.js";
class App extends React.Component {
  state = {
    open: true,
  };
  changeTheme = () => {
    this.setState(
      (val) => ({ open: !val.open }),
      () => {
        window.less.modifyVars(this.state.open ? light : dark);
      }
    );
  };
  render() {
    const { open } = this.state;
    return (
      <div className="container">
        <header>
          <button onClick={this.changeTheme}>{open ? "白" : "黑"}</button>
        </header>
        <section>所有子路由</section>
      </div>
    );
  }
}

export default App;
